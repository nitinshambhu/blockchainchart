package com.blockchain.base

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager;
import com.blockchain.ui.displayChart.DisplayChartFragment

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class BaseController @Inject constructor() {

    private lateinit var fragmentManager: FragmentManager


    fun setFragmentManager(fragmentManager: FragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    fun displayChart() {
        fragmentManager.beginTransaction()
            .replace(android.R.id.content, DisplayChartFragment())
            .commit();
    }
}
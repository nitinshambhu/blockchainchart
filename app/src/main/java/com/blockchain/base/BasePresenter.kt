package com.blockchain.base

open class BasePresenter<V> {

    internal var view: V? = null

    open fun attachView(view: V) {
        this.view = view
    }

    open fun detachView() {
        view = null
    }
}
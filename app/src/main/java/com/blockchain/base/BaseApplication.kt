package com.blockchain.base

import com.blockchain.dagger.component.DaggerAppComponent
import com.blockchain.dagger.module.ApiModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class BaseApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        return DaggerAppComponent.builder()
            .apiModule(ApiModule(AppConstants.BASE_URL))
            .create(this)
    }

}
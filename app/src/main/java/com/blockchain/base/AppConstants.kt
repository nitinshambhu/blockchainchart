package com.blockchain.base

object AppConstants {
    const val BASE_URL = "https://api.blockchain.info/"
    const val PATH_CHARTS: String = "charts"
}

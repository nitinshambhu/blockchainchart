package com.blockchain.util

import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import com.blockchain.R
import com.blockchain.databinding.DialogProgressBinding

fun Fragment.progressDialog(@StringRes resId: Int = R.string.fetching_data_please_wait): View {
    return DialogProgressBinding.inflate(LayoutInflater.from(activity)).root
}
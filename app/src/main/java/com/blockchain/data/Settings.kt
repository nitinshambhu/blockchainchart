package com.blockchain.data

import android.content.SharedPreferences
import javax.inject.Inject

class Settings @Inject constructor(private val editor: SharedPreferences.Editor) {

    fun save(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()
    }
}
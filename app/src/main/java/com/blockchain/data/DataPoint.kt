package com.blockchain.data

data class DataPoint(val x: Float = 0.toFloat(), val y: Float = 0.toFloat())
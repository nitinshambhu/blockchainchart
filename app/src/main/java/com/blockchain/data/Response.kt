package com.blockchain.data

data class Response(
    val name: String = "",
    val unit: String = "",
    val description: String = "",
    val values: List<DataPoint>? = null
)


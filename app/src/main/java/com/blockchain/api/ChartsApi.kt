package com.blockchain.api

import com.blockchain.base.AppConstants.PATH_CHARTS
import com.blockchain.data.Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ChartsApi {

    @GET("$PATH_CHARTS/transactions-per-second?")
    fun transactionsPerSecond(@Query("timespan") timespan: String, @Query("rollingAverage") rollingAverage: String, @Query("format") format: String = "json"): Single<Response>

    @GET("$PATH_CHARTS/market-price?")
    fun marketPrice(@Query("cors") cors: Boolean = true, @Query("format") format: String = "json", @Query("lang") lang: String = "en"): Single<Response>
}
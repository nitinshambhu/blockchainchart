package com.blockchain.ui.displayChart

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blockchain.base.BaseFragment
import com.blockchain.databinding.FragmentDisplayChartBinding
import com.blockchain.util.progressDialog
import com.blockchain.util.visible
import com.github.mikephil.charting.data.LineData
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class DisplayChartFragment : BaseFragment(), DisplayChartView {

    @Inject
    lateinit var presenter: DisplayChartPresenter

    val progressDialog: AlertDialog by lazy {
        AlertDialog.Builder(activity!!)
            .setView(progressDialog())
            .setCancelable(false)
            .create()
    }

    private lateinit var binding: FragmentDisplayChartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDisplayChartBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initGraph()
        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    fun initGraph() {
    }

    override fun showDialog(show: Boolean) {
        when (show) {
            true -> progressDialog.show()
            false -> progressDialog.dismiss()
        }
    }

    override fun showEmptyState(show: Boolean) {
        binding.emptyState.visible(show)
    }

    override fun showChart(show: Boolean) {
        binding.graph.visible(show)
    }

    override fun setText(resId: Int) {
        binding.emptyState.setText(resId)
    }

    override fun setData(data: LineData) {
        binding.graph.data = data
        binding.graph.invalidate()
    }
}
package com.blockchain.ui.displayChart

import android.support.annotation.StringRes
import com.github.mikephil.charting.data.LineData

interface DisplayChartView {
    fun showDialog(show: Boolean)
    fun showEmptyState(show: Boolean)
    fun showChart(show: Boolean)
    fun setText(@StringRes resId: Int)
    fun setData(data: LineData)
}
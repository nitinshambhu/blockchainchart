package com.blockchain.ui.displayChart

import com.blockchain.R
import com.blockchain.api.ChartsApi
import com.blockchain.base.BasePresenter
import com.blockchain.data.Response
import com.blockchain.data.DataPoint
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DisplayChartPresenter @Inject constructor(val chartsApi: ChartsApi) : BasePresenter<DisplayChartView>() {

    var disposable: Disposable? = null

    override fun attachView(view: DisplayChartView) {
        super.attachView(view)
        view.showDialog(true)
        disposable = chartsApi.marketPrice()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onChartDataFetchSuccess, this::onChartDataFetchError)
    }

    fun onChartDataFetchSuccess(response: Response) {
        view?.showChart(true)
        view?.showDialog(false)
        response.values?.apply { view?.setData(transformDataPointsToEntries(this, response.description)) }

    }

    fun onChartDataFetchError(throwable: Throwable) {
        view?.showEmptyState(true)
        view?.showDialog(false)
        view?.setText(R.string.no_data_found)
    }

    override fun detachView() {
        disposable?.dispose()
        disposable = null
        super.detachView()
    }

    fun transformDataPointsToEntries(list: List<DataPoint>, description: String): LineData {
        return LineData(LineDataSet(list.asSequence().map { Entry(it.x, it.y) }.toList(), description))
    }
}
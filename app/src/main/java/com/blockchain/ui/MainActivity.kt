package com.blockchain.ui

import android.os.Bundle
import com.blockchain.base.BaseActivity
import com.blockchain.base.BaseController
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var controller: BaseController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        controller.setFragmentManager(supportFragmentManager)
        controller.displayChart()
    }
}

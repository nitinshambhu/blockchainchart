package com.blockchain.dagger.component


import com.blockchain.base.BaseApplication
import com.blockchain.dagger.module.ApiModule
import com.blockchain.dagger.module.AppModule
import com.blockchain.dagger.module.DataModule

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ApiModule::class, AppModule::class, DataModule::class])
interface AppComponent : AndroidInjector<BaseApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<BaseApplication>() {
        abstract fun apiModule(module: ApiModule): Builder
        abstract override fun build(): AppComponent
    }
}
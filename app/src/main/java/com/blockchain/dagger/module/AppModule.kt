package com.blockchain.dagger.module

import android.app.Application
import dagger.android.ContributesAndroidInjector
import com.blockchain.ui.MainActivity
import com.blockchain.base.BaseActivity
import com.blockchain.base.BaseApplication
import com.blockchain.ui.displayChart.DisplayChartFragment
import dagger.Binds
import dagger.Module


@Module
abstract class AppModule {

    @Binds
    internal abstract fun application(application: BaseApplication): Application

    @ContributesAndroidInjector
    internal abstract fun baseActivity(): BaseActivity

    @ContributesAndroidInjector
    internal abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun displayChartFragment(): DisplayChartFragment
}
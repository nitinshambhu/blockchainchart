package com.blockchain.dagger.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import javax.inject.Singleton
import dagger.Provides
import com.google.gson.Gson
import android.preference.PreferenceManager
import com.blockchain.data.Settings
import dagger.Module


@Module
class DataModule {

    @Provides
    @Singleton
    internal fun providesContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun providesSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @Singleton
    internal fun providesSharedPreferencesEditor(prefs: SharedPreferences): SharedPreferences.Editor {
        return prefs.edit()
    }

    @Provides
    @Singleton
    internal fun providesGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    internal fun providesSettings(editor: SharedPreferences.Editor): Settings {
        return Settings(editor)
    }
}
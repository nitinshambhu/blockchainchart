package com.blockchain.dagger.module

import com.blockchain.api.ChartsApi
import dagger.Module

import javax.inject.Singleton;

import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
open class ApiModule constructor(private val baseUrl: String) {

    @Provides
    @Singleton
    fun providesOkHttpClientBuilder(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(logging)
        return clientBuilder.build()
    }

    @Provides
    @Singleton
    fun providesRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
    }

    @Singleton
    @Provides
    fun providesRetrofit(builder: Retrofit.Builder, client: OkHttpClient): Retrofit {
        return builder.client(client).build()
    }

    @Singleton
    @Provides
    fun providesChartApi(retrofit: Retrofit): ChartsApi {
        return retrofit.create(ChartsApi::class.java)
    }
}

package com.blockchain.ui.displayChart

import com.blockchain.R
import com.blockchain.api.ChartsApi
import com.blockchain.data.DataPoint
import com.blockchain.data.Response
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Test

class DisplayChartPresenterTest {

    val view: DisplayChartView = mock()
    val chartsApi: ChartsApi = mock()
    val presenter = spy(DisplayChartPresenter(chartsApi = chartsApi))

    @Before
    fun setUp() {
        RxJavaPlugins.setComputationSchedulerHandler { _ -> Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
    }

    @After
    fun tearDown() {
        reset(chartsApi)
        reset(view)
        reset(presenter)
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    @Test
    fun testAttachViewWithMarketPriceFetchSuccess() {
        val response: Response = mock()
        doNothing().`when`(presenter).onChartDataFetchSuccess(any())
        doNothing().`when`(presenter).onChartDataFetchError(any())
        whenever(chartsApi.marketPrice()).thenReturn(Single.just(response))
        assertNull(presenter.disposable)

        presenter.attachView(view)

        assertNotNull(presenter.disposable)
        verify(view, times(1)).showDialog(true)
        verify(chartsApi, times(1)).marketPrice()
        verify(presenter, times(0)).onChartDataFetchSuccess(any())
        verify(presenter, times(0)).onChartDataFetchError(any())
    }

    @Test
    fun testAttachViewWithMarketPriceFetchError() {
        doNothing().`when`(presenter).onChartDataFetchSuccess(any())
        doNothing().`when`(presenter).onChartDataFetchError(any())
        whenever(chartsApi.marketPrice()).thenReturn(Single.error(Throwable()))
        assertNull(presenter.disposable)

        presenter.attachView(view)

        assertNotNull(presenter.disposable)
        verify(view, times(1)).showDialog(true)
        verify(chartsApi, times(1)).marketPrice()
        verify(presenter, times(0)).onChartDataFetchSuccess(any())
        verify(presenter, times(1)).onChartDataFetchError(any())
    }

    @Test
    fun testDetachView() {
        presenter.view = view
        presenter.disposable = CompositeDisposable()

        presenter.detachView()

        assertNull(presenter.view)
        assertNull(presenter.disposable)
    }

    @Test
    fun testOnChartDataFetchSuccess() {
        val lineData = LineData()
        doReturn(lineData).`when`(presenter).transformDataPointsToEntries(any(), any())
        val response: Response = mock {
            on { description } doReturn "test"
        }
        presenter.view = view

        presenter.onChartDataFetchSuccess(response)

        verify(view, times(1)).showChart(true)
        verify(view, times(1)).showDialog(false)
        verify(presenter, times(1)).transformDataPointsToEntries(any(), eq("test"))
        verify(view, times(1)).setData(lineData)
        verify(view, times(0)).showEmptyState(true)
    }

    @Test
    fun testOnChartDataFetchError() {
        presenter.view = view

        presenter.onChartDataFetchError(Throwable())

        verify(view, times(1)).showEmptyState(true)
        verify(view, times(1)).showDialog(false)
        verify(view, times(1)).setText(R.string.no_data_found)
        verify(view, times(0)).showChart(false)
    }

    @Test
    fun testTransformDataPointsToEntries() {
        val dataPoint: DataPoint = mock {
            on { x } doReturn 1.0f
            on { y } doReturn 2.0f
        }
        val entry = Entry(1.0f, 2.0f)

        val actual = presenter.transformDataPointsToEntries(listOf(dataPoint), "test")

        assertEquals(1, actual.dataSets.size)
        assertEquals(entry.x, actual.dataSets[0].getEntryForIndex(0).x)
        assertEquals(entry.y, actual.dataSets[0].getEntryForIndex(0).y)
    }
}
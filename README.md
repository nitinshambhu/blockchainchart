#BlockChainChart

##Disclaimers

* I am travelling to India on Friday night. I will reach there Saturday evening. I will be back only after 2 weeks i.e November 25. I expressed my inability to Laia Aleu to be able to spend 2 to 3 days time on the coding challenge. I was advised to do the best I could in the available time. I just have today�s (Thursday) evening to finish it. So, I spent 3 hours to create this app.
* I am not a UX Designer. In the real world, we generally have UX Designers. At least that's the case with all the companies I worked for so far
* I only played the role of a developer who focused more on structuring the code, testing it and keeping it modular rather than focusing on fancy UI

##Technologies Used

* Kotlin
* Dagger to inject the dependencies
* Retrofit for network calls
* RxJava to make the app reactive to the fetched data
* DataBinding
* Gson

##Design Patterns Used

* Standard MVP pattern
* As the app involves just 1 network call, I used ChartsApi as the model
* Dependency inversion principle to define relation between Fragment and Presenter

##Implemented

* A working app that fetches the market price of the Bit coin and displays its graph
* [Screenshot](https://bitbucket.org/nitinshambhu/blockchainchart/src/master/screenshots/Graph.png)
* All the dependencies are injected using Dagger. Dependencies are always injected & never created inside a class
* Unit test cases are written only for presenter to show case my habit of writing test cases

##Possible Improvements

* Unit test cases for DisplayChartFragment as well
* Espresso tests could also have written
* The chart could use some styling

